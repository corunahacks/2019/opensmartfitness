# Open Smart Fitness
Control your fitness activity with freedom

## WHAT IS THIS PROJECT

This project was born in Coruña Hacks 2019. The initial idea is to develop a free and open platform in order to control the personal fitness information. The project includes the sensors designs and implementations, as well as a mobile application to store, show and manage the information about the fitness sessions, using only open-source technologies both for hardware and software.

The first stage is focused on delivering information about one of the most important cycling performance metrics: the pedaling rate. In terms of hardware, there are three main parts involved:

1. *A magnet*, placed in the pedal.
2. *A [Hall effect sensor](https://en.wikipedia.org/wiki/Hall_effect_sensor)*, located in the bicycle frame, which is able to read when the pedal passes through a certain position.
3. *A [ESP32 board](https://www.esp32.com/)*, which reads the Hall effect sensor signal and calculates the cadence by measuring the duration between the last two values. The board is also equipped with a bluetooth module, which allows us sending those metrics to another device.

To gather and display the information emitted by the aforementioned setup, we built a mobile app with [NativeScript](https://github.com/NativeScript/NativeScript) and [Vue.js](https://github.com/vuejs/vue), so that by using JavaScript and CSS we can deliver a native-like experience for both Android and iOS devices.

## HOW TO BUILD

### Sensors
You can see below a circuit diagram that depicts how to assemble the setup:

![](sensors/candenceSensorDiagram.png)

The board firmware is in the [sensors/cadenceBLESensor/cadenceBLESensor.ino](sensors/cadenceBLESensor/cadenceBLESensor.ino) file, writen in the Arduino programming language.

### Mobile App

#### Prerequisites

* Android SDK
* Android Support Repository
* Android SDK Build-tools 28.0.3 or a later stable official release
* Node.js: Download and install the latest LTS version of Node.js. Restart your terminal and verify the installation was successful by running node --version.
* NativeScript CLI: open your terminal and run `npm install -g nativescript`

#### Set-up
Clone this repository:

```
$ git clone https://gitlab.com/corunahacks/2019/opensmartfitness.git
```

Install the dependencies:

```
$ cd opensmartfitness/mobile/app-native-script
$ npm install
```

Connect an Android device to your computer and build the project:

```
$ tns run android --bundle
```

And that's it! NativeScript will be watching for changes, so you can code and see your changes automatically deployed in your device.

### Project Structure
The mobile app code resides in the [/mobile/app-native-script](mobile/app-native-script). It uses [NativeScript-Vue](https://nativescript-vue.org/), and the key files and folders are:

* [app/components](mobile/app-native-script/app/components): this folder contains the UI components. They are `Vue` files, which get eventually transpiled to native components.
* [app/components/App.vue](mobile/app-native-script/app/components/App.vue): this is the main UI component of the app, that holds the definition of the primary view, as well as some business logic. This file that you'll modify to change the behavior of the app.
* [app/app.scss](mobile/app-native-script/app/app.scss): file containing global CSS definitions.


## TROUBLESHOOTING

### Mobile App
If you're having any trouble while setting up the mobile app, please make sure that you have the following dependencies:

* G++ compiler
* JDK 8

You must also have the following two environment variables setup for Android development:

* `JAVA_HOME`
* `ANDROID_HOME`

If you need more help, you can refer to [the NativeScript setup guide](https://docs.nativescript.org/start/ns-setup-linux#system-requirements)

## CODING STANDARDS

TO-DO

## CONTRIBUTORS
* @alopezf
* @braisarias
* @davidgf
* @m.castrovilarino

## LICENSE

This project is distributed under the [GNU GENERAL PUBLIC LICENSE Version 3](LICENSE).
