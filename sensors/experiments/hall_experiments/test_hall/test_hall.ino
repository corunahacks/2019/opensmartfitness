
const byte ledPinGreen = 5;
const byte ledPinRed = 3;
const byte interruptPin = 2;
volatile byte state = LOW;
int val=0;


void setup() {
  pinMode(ledPinGreen, OUTPUT);
  pinMode(ledPinRed, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), test, CHANGE);
  Serial.begin(115200);
  Serial.println("Hall test configured!");
}

void loop() {
  digitalWrite(ledPinGreen, !state);
  digitalWrite(ledPinRed, state);
  //delay(1000);
  Serial.println(val/2);
}

void test() {
  Serial.println("test");
  Serial.print(" old state: ");
  Serial.print(state);
  state = !state;
  Serial.print(" new state: ");
  Serial.print(state);
  val++;
  Serial.print("\n");
}
