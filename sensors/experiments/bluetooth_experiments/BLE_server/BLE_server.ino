#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

#define SERVICE_UUID        "ad2b0316-4851-48a9-aebd-42bedc3654bd"
#define CHARACTERISTIC_UUID_RX "859a384b-a782-4b36-b9a1-8f051993ee65"
#define CHARACTERISTIC_UUID_TX "45d4f28f-b24a-4619-83d1-be0a2316577b"

int counter=0;
#define MAX_BUF 200
char myBuf[MAX_BUF];
String strValue;

bool deviceConnected = false;

BLECharacteristic *pCharacteristicTX;

class ServerCallbacks: public BLEServerCallbacks {
  void onConnect(BLEServer* pServer) {
    //TO-DO Maybe print some log
    deviceConnected = true;
  };
  void onDisconnect(BLEServer* pServer) {
    //TO-DO Maybe print some log
    deviceConnected = false;
  }
};


class CharacteristicCallbacks: public BLECharacteristicCallbacks {
     void onWrite(BLECharacteristic *characteristic) {
      std::string rxValue = characteristic->getValue();
      if (rxValue.length() > 0) {
        Serial.println("onWrite. Received:");
        for (int i=0; i<rxValue.length(); i++) {
          Serial.print(rxValue[i]);
        }
        Serial.println("\nend onWrite");
      }
     }
};

void setup() {
  Serial.begin(115200);
  Serial.println("Starting BLE work!");

  // ceate BLE device with name
  BLEDevice::init("OpenSmartFitness");
  
  BLEServer *pServer = BLEDevice::createServer();

  pServer->setCallbacks(new ServerCallbacks());
  
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // create characteristics
  pCharacteristicTX = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID_TX,
                                         BLECharacteristic::PROPERTY_NOTIFY
                                       );             
  pCharacteristicTX->addDescriptor(new BLE2902());


  BLECharacteristic *characteristicRX = pService->createCharacteristic(
                                          CHARACTERISTIC_UUID_RX,
                                          BLECharacteristic::PROPERTY_WRITE
                                        );

  characteristicRX->setCallbacks(new CharacteristicCallbacks());


  
  pService->start();

  

  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
  pServer->getAdvertising()->start();
  Serial.println("Characteristic defined! Waiting for connection...");
}

int getCounter(){
  if (counter++ < 100){
    return counter;
  }
  return counter=0;
}

void loop() {
    if (deviceConnected) {
      strValue = String( "{\"value\":\""  + String(getCounter()) + "\"}\0");
      Serial.print(" Value: ");
      Serial.print(counter);
      Serial.print(" Enviado: ");
      Serial.println(strValue);
      
      strValue.toCharArray(myBuf, MAX_BUF);
      pCharacteristicTX->setValue(myBuf);
      pCharacteristicTX->notify();
      
    } else {
      Serial.print(".");
    }
  delay(2000);
}
