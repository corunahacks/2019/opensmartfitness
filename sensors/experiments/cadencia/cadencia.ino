#define INIT 1
#define WORK 0
#define INTER 2
#define MAX_BUFFER 100

const unsigned long milliSecondsInMinute = 60000;
const byte ledPinGreen = 5;
const byte ledPinRed = 3;
const byte interruptPin = 2;
int val=0;

int mode;
int state;

unsigned int bufferCounter=0;
unsigned int indexCounter=0;
unsigned long buffer[MAX_BUFFER];

unsigned long revolutionCount=0;
unsigned long lastRevolutionStartTime = 0;
unsigned long thisRevolutionStartTime = 0;
unsigned long currentDuration;
unsigned long currentTime;
unsigned long revolutionTime = 0;

unsigned long maxRevolutionTime=0;
unsigned long minRevolutionTime=0;

double rpm, rpm_inst;

void setup() {
  mode = INIT;
  state = INIT;
  pinMode(ledPinGreen, OUTPUT);
  pinMode(ledPinRed, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), revolution, CHANGE);
  Serial.begin(115200);
  Serial.println("Configured!");
}

void printData(){
  Serial.print(" RPM_inst: ");
  Serial.print(rpm_inst);
  Serial.print(" | RPM: ");
  Serial.print(rpm);
  Serial.print(" | max: ");
  Serial.print(maxRevolutionTime);
  Serial.print(" | min: ");
  Serial.print(minRevolutionTime);
  Serial.print(" | t: ");
  Serial.print(revolutionTime);

  Serial.print("\n");
}


void loop() {

  delay(1000); 
  if (mode == WORK){
    calculeRPMinst();
    printData();
  }
  
}

void calculeRPMinst(){
  rpm_inst = milliSecondsInMinute / (millis() - lastRevolutionStartTime);
}

void calculeData(){
  revolutionTime = thisRevolutionStartTime - lastRevolutionStartTime;
  
  if (mode == INIT){
    maxRevolutionTime = minRevolutionTime = revolutionTime;
    mode = WORK;
  }

  if (revolutionTime > maxRevolutionTime){
    maxRevolutionTime = revolutionTime;
  }

  if (revolutionTime < minRevolutionTime){
    minRevolutionTime = revolutionTime;
  }

  rpm = milliSecondsInMinute / revolutionTime;
  rpm_inst = rpm;

  lastRevolutionStartTime = thisRevolutionStartTime;
}

void revolution() {
  if ((state == INTER) || (currentTime + 200 >= millis())){
    // abort because we are in another interruption
    return;
  }
  state = INTER;
  currentTime = millis();
  Serial.print("REV - t = ");
  thisRevolutionStartTime = currentTime;
  Serial.println(currentTime);

  calculeData();

  state = WORK;
}
