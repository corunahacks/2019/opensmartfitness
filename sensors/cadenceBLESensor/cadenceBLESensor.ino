// Copyright (C) 2019 OpenSmartFitness contributors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>


#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

#define SERVICE_UUID        "ad2b0316-4851-48a9-aebd-42bedc3654bd"
#define CHARACTERISTIC_UUID_RX "859a384b-a782-4b36-b9a1-8f051993ee65"
#define CHARACTERISTIC_UUID_TX "45d4f28f-b24a-4619-83d1-be0a2316577b"

#define INIT 1
#define WORK 0
#define INTER 2
#define MAX_BUFFER 100

int counter=0;
#define MAX_BUF 200
char myBuf[MAX_BUF];
String strValue;

const unsigned long milliSecondsInMinute = 60000;
const byte interruptPin = 13;
int val=0;

int mode;
int state;

unsigned int bufferCounter=0;
unsigned int indexCounter=0;
unsigned long buffer[MAX_BUFFER];

unsigned long revolutionCount=0;
unsigned long lastRevolutionStartTime = 0;
unsigned long thisRevolutionStartTime = 0;
unsigned long currentDuration;
unsigned long currentTime;
unsigned long revolutionTime = 0;

unsigned long maxRevolutionTime=0;
unsigned long minRevolutionTime=0;

double rpm, rpm_inst;

bool deviceConnected = false;

BLECharacteristic *pCharacteristicTX;

class ServerCallbacks: public BLEServerCallbacks {
  void onConnect(BLEServer* pServer) {
    //TO-DO Maybe print some log
    deviceConnected = true;
  };
  void onDisconnect(BLEServer* pServer) {
    //TO-DO Maybe print some log
    deviceConnected = false;
  }
};


class CharacteristicCallbacks: public BLECharacteristicCallbacks {
     void onWrite(BLECharacteristic *characteristic) {
      std::string rxValue = characteristic->getValue();
      if (rxValue.length() > 0) {
        Serial.println("onWrite. Received:");
        for (int i=0; i<rxValue.length(); i++) {
          Serial.print(rxValue[i]);
        }
        Serial.println("\nend onWrite");
      }
     }
};

void setup() {
  Serial.begin(115200);
  Serial.println("Starting OpenSmartFitness Cadence Sensor BLE work!");
  mode = INIT;
  state = INIT;

  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), revolution, CHANGE);

  // ceate BLE device with name
  BLEDevice::init("OpenSmartFitness");
  
  BLEServer *pServer = BLEDevice::createServer();

  pServer->setCallbacks(new ServerCallbacks());
  
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // create characteristics
  pCharacteristicTX = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID_TX,
                                         BLECharacteristic::PROPERTY_NOTIFY
                                       );             
  pCharacteristicTX->addDescriptor(new BLE2902());


  BLECharacteristic *characteristicRX = pService->createCharacteristic(
                                          CHARACTERISTIC_UUID_RX,
                                          BLECharacteristic::PROPERTY_WRITE
                                        );

  characteristicRX->setCallbacks(new CharacteristicCallbacks());


  
  pService->start();

  

  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
  pServer->getAdvertising()->start();
  Serial.println("Configured! Waiting for connection...");
}

void printData(){
  Serial.print(" RPM_inst: ");
  Serial.print(rpm_inst);
  Serial.print(" | RPM: ");
  Serial.print(rpm);
  Serial.print(" | max: ");
  Serial.print(maxRevolutionTime);
  Serial.print(" | min: ");
  Serial.print(minRevolutionTime);
  Serial.print(" | t: ");
  Serial.print(revolutionTime);

  Serial.print("\n");
}

void sendDataBLE(){
  // check if  device conected
  if (deviceConnected) {
    // uncomment to send the timestamp too
    //strValue = String( "{\"cadence\":"  + String(rpm) + ",\"t\":" + String(thisRevolutionStartTime) + "}\0");
    strValue = String( "{\"cadence\":"  + String(rpm) + "}\0");
    Serial.print(" Cadence: ");
    Serial.print(rpm);
    Serial.print(" Enviado: ");
    Serial.println(strValue);
        
    strValue.toCharArray(myBuf, MAX_BUF);
    pCharacteristicTX->setValue(myBuf);
    pCharacteristicTX->notify();
  }
}

int getCounter(){
  if (counter++ < 100){
    return counter;
  }
  return counter=0;
}

void calculeRPMinst(){
  rpm_inst = milliSecondsInMinute / (millis() - lastRevolutionStartTime);
}

void calculeData(){
  revolutionTime = thisRevolutionStartTime - lastRevolutionStartTime;
  
  if (mode == INIT){
    maxRevolutionTime = minRevolutionTime = revolutionTime;
    mode = WORK;
  }

  if (revolutionTime > maxRevolutionTime){
    maxRevolutionTime = revolutionTime;
  }

  if (revolutionTime < minRevolutionTime){
    minRevolutionTime = revolutionTime;
  }

  rpm = milliSecondsInMinute / revolutionTime;
  rpm_inst = rpm;

  lastRevolutionStartTime = thisRevolutionStartTime;
}

void revolution() {
  if ((state == INTER) || (currentTime + 200 >= millis())){
    // abort because we are in another interruption
    return;
  }
  state = INTER;
  currentTime = millis();
  Serial.print("REV - t = ");
  thisRevolutionStartTime = currentTime;
  Serial.println(currentTime);

  calculeData();
  
  sendDataBLE();

  state = WORK;
}

void loop() {
    //if (deviceConnected) {
      //strValue = String( "{\"value\":\""  + String(getCounter()) + "\"}\0");
      //Serial.print(" Value: ");
      //Serial.print(counter);
      //Serial.print(" Enviado: ");
      //Serial.println(strValue);
      
      //strValue.toCharArray(myBuf, MAX_BUF);
      //pCharacteristicTX->setValue(myBuf);
      //pCharacteristicTX->notify();
      
    //} else {
      //Serial.print(".");
    //}

    if (mode == WORK){
      calculeRPMinst();
      printData();
    }
  delay(2000);
}
