// @flow

import { put, call } from "redux-saga/effects"
import { Device, Service, Characteristic, BleError } from "react-native-ble-plx"
import { log, logError, listenSpeed } from "./Reducer"

export type SensorTagTestMetadata = {
  id: string,
  title: string,
  execute: (device: Device) => Generator<any, boolean, any>
}

export const SensorTagTests: { [string]: SensorTagTestMetadata } = {
  READ_ALL_CHARACTERISTICS: {
    id: "READ_ALL_CHARACTERISTICS",
    title: "Read all characteristics",
    execute: readAllCharacteristics
  },
  READ_SPEED: {
    id: "READ_SPEED",
    title: "Read speed",
    execute: readSpeed
  }
}

function* readAllCharacteristics(device: Device): Generator<*, boolean, *> {
  try {
    const services: Array<Service> = yield call([device, device.services])
    for (const service of services) {
      yield put(log("Found service: " + service.uuid))
      const characteristics: Array<Characteristic> = yield call([
        service,
        service.characteristics
      ])
      for (const characteristic of characteristics) {
        if (characteristic.uuid == "00002a02-0000-1000-8000-00805f9b34fb")
          continue

        yield put(log("Found characteristic: " + characteristic.uuid))
        if (characteristic.isReadable) {
          yield put(log("Reading value..."))
          var c = yield call([characteristic, characteristic.read])
          yield put(log("Got base64 value, decoded: " + characteristic))
          if (characteristic.isWritableWithResponse) {
            yield call(
              [characteristic, characteristic.writeWithResponse],
              c.value
            )
            yield put(log("Successfully written value back"))
          }
        }
      }
    }
  } catch (error) {
    yield put(logError(error))
    return false
  }

  return true
}

const serviceUuid = 'ad2b0316-4851-48a9-aebd-42bedc3654bd'
const speedCharUuid = '45d4f28f-b24a-4619-83d1-be0a2316577b'

function* readSpeed(device: Device): Generator<*, boolean, *> {
  try {
    const services: Array<Service> = yield call([device, device.services])
    const servicesUids = services.map(s => s.uuid)
    const servicesIds = services.map(s => s.id)
    const devicesIds = services.map(s => s.deviceID)
    const service = services.find(s => s.uuid === serviceUuid)
    if (service) {
      yield put(log('SERVICE FOUND'))
      const characteristics: Array<Characteristic> = yield call([
        service,
        service.characteristics
      ])
      yield put(log("Characteristic UUIDS: " + characteristics.map(c => c.uuid).join('     ')))
      const speedChar = characteristics.find(el => el.uuid.toLowerCase() === speedCharUuid.toLowerCase())
      if (speedChar) {
        yield put(log('SPEEEEEEEED FOUND'))
        yield put(listenSpeed(speedChar))
      } else {
        yield put(log('SPEED NOT FOUND'))
      }
    } else {
      yield put(log('SERVICE NOT FOUND'))
    }
  } catch (error) {
    yield put(logError(error))
    return false
  }

  return true
}
